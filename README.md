![title image.jpg](https://bitbucket.org/repo/akyGAMb/images/3354667133-title%20image.jpg)
# README #
## Use cases ##
* Trigger voice recognition on the Raspberry Pi clone of Amazon Echo

![ Image of a Amazon Echo and Echo Dot ](https://bitbucket.org/repo/akyGAMb/images/1453684613-echo_feature._CB526147196_.png)

* Use as a doorbell

![il_fullxfull.238326233.jpg](https://bitbucket.org/repo/akyGAMb/images/922146569-il_fullxfull.238326233.jpg)
## Requirements ##
* Raspberry Pi
* Flic
* Flic SDK for Linux

## How to use? ##
* Open terminal and run ***addflic*** to connect a button to the pi
* Reboot
* Add files to a playlist with the following filenames:

1. single_announcement.mp3 - for single click
2. double_announcement.mp3 - for double click
3. hold_announcement.mp3 - for 2 second hold