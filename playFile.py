from time import sleep
import alsaaudio as alsa
import glob
from  os import chdir
import subprocess

mytvPlayerVolumeControl = 'Main volume'

m = alsa.Mixer(mytvPlayerVolumeControl)
mp3RootPath = '/home/pi/mytvpyclient/files/'

def fadeIn():
    for vol in range(0, 101, 1):
        sleep(0.02)
        m.setvolume(vol)

def fadeOut():
    for vol in range(100, -1, -1):
        sleep(0.02)
        m.setvolume(vol)

def findPlayFile(clickType):
    chdir(mp3RootPath)
    for file in glob.glob('*' + clickType + '.announcement.mp3'):
        return file

def playAnnouncement(clickType):
    fileToPlay = findPlayFile(clickType)
    if fileToPlay:
        mp3Path = mp3RootPath + fileToPlay
        fadeOut()
        sleep(0.5)
        subprocess.call(['omxplayer', '-o', 'alsa:secondvolume', '--no-osd', '--no-keys', mp3Path])
        fadeIn()
