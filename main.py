#!/usr/bin/python3

import sys, os

home_path = os.path.expanduser('~')
fliclib_path = './fliclib-linux-hci/clientlib/python'
sys.path.append(home_path + fliclib_path)

import fliclib
import playFile

client = fliclib.FlicClient("localhost")
playFile = playFile

def got_button(bd_addr):
    cc = fliclib.ButtonConnectionChannel(bd_addr)
    cc.on_button_single_or_double_click_or_hold = \
        lambda channel, click_type, was_queued, time_diff: \
            clickAction(click_type, was_queued)

    cc.on_connection_status_changed = \
        lambda channel, connection_status, disconnect_reason: \
            print(channel.bd_addr + " " + str(connection_status) + (" " + str(disconnect_reason) if connection_status == fliclib.ConnectionStatus.Disconnected else ""))
    client.add_connection_channel(cc)

def got_info(items):
    print(items)
    for bd_addr in items["bd_addr_of_verified_buttons"]:
        got_button(bd_addr)

def clickAction(click_type, was_queued):
    if not was_queued:
        if str(click_type) == "ClickType.ButtonDoubleClick":
            print("Double")
            playFile.playAnnouncement('double')
        if str(click_type) == "ClickType.ButtonHold":
            print("Hold")
            playFile.playAnnouncement('hold')
        if str(click_type) == "ClickType.ButtonSingleClick":
            print("Single")
            playFile.playAnnouncement('single')

client.get_info(got_info)

client.on_new_verified_button = got_button

client.handle_events()
